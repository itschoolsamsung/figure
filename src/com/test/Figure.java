package com.test;

public class Figure {
    private double a;
    private double b;
    private double c = 0;
    private boolean isTriangle = false;

    Figure(double a) {
        this(a, a);
    }

    Figure(double a, double b) {
        setA(a);
        setB(b);
    }

    Figure(double a, double b, double c) {
        setA(a);
        setB(b);
        setC(c);

        if ((this.a + this.b <= this.c) || (this.a + this.c <= this.b) || (this.b + this.c <= this.a)) {
            setA(1);
            setB(1);
            setC(1);
        }

        isTriangle = true;
    }

    void setA(double a) {
        this.a = (0 == a) ? 1 : a;
    }

    void setB(double b) {
        this.b = (0 == b) ? 1 : b;
    }

    void setC(double c) {
        this.c = (0 == c) ? 1 : c;
    }

    double perimeter() {
        return a + b + c;
    }

    double area() {
        if (isTriangle) {
            double p = (a+b+c) / 2;
            return Math.sqrt(p * (p-a) * (p-b) * (p-c));
        }

        return a * b;
    }

    void print() {
        System.out.printf("perimeter=%.2f, area=%.2f\n", perimeter(), area());
    }
}
