package com.test;

public class Test {
    public static void main(String[] args) {
        new Figure(2).print();
        new Figure(2, 3).print();
        new Figure(3, 4, 5).print();
    }
}
